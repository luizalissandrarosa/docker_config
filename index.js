const express = require('express');

const PORT = 3000;

/* Para que o docker entenda que o que deve fazer
é apenas o repasso da porta 3000 para a máquina. */
const HOST = '0.0.0.0';

const app = express();

app.get('/', (request, response) => {

  response.send("Hello, World!");

});

app.listen(PORT, HOST);